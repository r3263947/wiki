# GitLab best practices

1. [Resolving merge request comments](#resolve-merge-request-comments)
1. [Use top level comments to signify updates](#use-top-level-comments-to-signify-updates)
1. [Don't use top level comments](#dont-use-top-level-comments)
1. [Explicitly mention people](#explicitly-mention-people)
1. [One comment per logical entity](#one-comment-per-logical-entity)
1. [Beware of using temporary commit messages when squash merging](#beware-of-using-temporary-commit-messages-when-squash-merging)
1. [Reference wiki pages without the file extension](#reference-wiki-pages-without-the-file-extension)

## Resolve merge request comments

Resolving merge request comments is a great way to keep track of comments, but it's important to keep a few things in mind when using this workflow. Resolving a comment doesn't trigger a notification. Without a notification the commentor might not notice that his comment has been resolved, he might miss his chance to object the resolution. Instead you should leave a comment detailing how the comment was addressed (a simple `Done` might be sufficient) but leave the comment un-resolved. The commenter then should resolve the comment if the resolution is sufficient or continue the discussion in case it's not.

## Use top level comments to signify updates

Since GitLab doesn't ([yet](https://gitlab.com/groups/gitlab-org/-/epics/23)) support drafting comments and instead will send a notification for each comment there is no way to tell if the implementor is waiting for your feedback or still busy addressing comments. To help with that it makes sense to create a top level comment detailing the changes and [mentioning](#explicitly-mention-people) the other party whenever a review is done or a new version waiting for feedback is available.

## Don't use top level comments

Top level comments are not [resolve-able](#resolve-merge-request-comments) so use them only for things that change the state of the merge request (e.g. [signify updates](#use-top-level-comments-to-signify-updates)) and not for things that need to be addressed.

## Explicitly mention people

Explicitly mention people, even if it's obvious from the conversation, to trigger the creation of a [todo](https://docs.gitlab.com/ce/workflow/todos.html) entry. This way people have an easy way to keep track of things that need their attention even if they miss the email notification.

## One comment per logical entity

Since each comment can be resolved individually it makes sense to restrict each comment to a single logical entity.

## Beware of using temporary commit messages when squash merging

According to GitLab's [manual](https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html#commit-metadata-for-squashed-commits) when squash merging a merge request via the UI the merge request title (not the commit message of any one of the commits) will be used as the commit message for the new commit. However not all merges result in a squash so you cannot blindly rely on this feature to determine the right commit message. When the merge request only contains a single commit and is fast-forward able no squashing will take place and the commit message of the single commit will be used instead (because the commit will be used as is since it's fast-forward able). If you used a temporary commit message you'll end up with the wrong commit message in the target branch.

## Reference wiki pages without the file extension

Due to an [unexpected behavior](https://gitlab.com/gitlab-org/gitlab/-/issues/17845), we shouldn't add the file extension (`.md`) when linking to wiki's pages. Otherwise we will be redirected to the markdown source code instead of the rendered version.

